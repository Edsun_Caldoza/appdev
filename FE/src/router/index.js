import Vue from 'vue'
 import VueRouter from 'vue-router'
 import Dashboard from '../components/pages/index/Dashboard'
 
 Vue.use(VueRouter)
 
 const routes = [
  {
    path: '/login',
    name: 'login',
    component: () => import(/* webpackChunkName: "login" */ '@/components/pages/login/Login')
  },

   {
     path: '/',
     name: 'Dashboard',
     component: Dashboard
   },
   
   {
     path: '/books',
     name: 'Books Management',
     component: () => import(/* webpackChunkName: "books" */ '@/components/pages/books/books')
   },
   {
     path: '/patron',
     name: 'Patron Management',
     component: () => import(/* webpackChunkName: "patron" */ '@/components/pages/patron/Patron')
   },

   {
    path: '/borrowed/book',
    name: 'Borrowed Book',
    component: () => import(/* webpackChunkName: "patron" */ '@/components/pages/borrowedbook/BorrowedBook')
  },
  {
    path: '/returned/book',
    name: 'Returned Book',
    component: () => import(/* webpackChunkName: "patron" */ '@/components/pages/returnedbook/ReturnedBook')
  },
  {
    path: '/info',
    name: 'info',
    component: () => import(/* webpackChunkName: "login" */ '@/components/pages/info/Info')
  },
  {
    path: '/student',
    name: 'Student Dashboard',
    component: () => import(/* webpackChunkName: "login" */ '@/components/pages/student/Dashboard')
  },

 ]
 
 const router = new VueRouter({
   mode: 'history',
   base: process.env.BASE_URL,
   routes
})

export default router