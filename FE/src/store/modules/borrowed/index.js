import Vue from 'vue'
import Vuex from 'vuex'
import AXIOS from '../../config'

Vue.use(Vuex);

const borrowedbook = 'borrowedbook'

export default ({
  namespaced: true,
  state: {
    borrowedbook: []
  },
  actions: {
    async createBorrowed({commit}, {data, index}){
      const res = await AXIOS.post(`${borrowedbook}`, data)
      .then(response => {
        commit('books/UPDATE_COPIES', {id: index, data: response.data.borrowedbook.book.copies}, {root: true})
        return response
      })
      .catch(error => {
        return error.response
      })

      return res;
    },
    async getBorrowedBook({ commit }) {
      const res = await AXIOS.get(borrowedbook).then(response => {
          commit('SET_borrowedbook', response.data)
          return response
      }).catch(error => {
          return error.response;
      })
      return res
  },
  },
  getters: {
    getBorrowedBooksData(state) {
      return state.borrowedbook;
  }
  },
  mutations: {
    SET_borrowedbook(state, borrowedbook) {
      state.borrowedbook = borrowedbook;
  },
  },
})