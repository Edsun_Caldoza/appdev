import Vue from 'vue'
import Vuex from 'vuex'
import AXIOS from '../../config'

Vue.use(Vuex);

const returnedbook = 'returnedbook'

export default ({
  namespaced: true,
  state: {
    returnedbook: []
  },
  actions: {
    async createReturned({commit}, {data, index}){
      const res = await AXIOS.post(`${returnedbook}`, data)
      .then(response => {
        commit('books/UPDATE_COPIES', {id: index, data: response.data.returnedbook.book.copies}, {root: true})
        return response
      })
      .catch(error => {
        return error.response
      })

      return res;
    },
    async getReturnedBook({ commit }) {
      const res = await AXIOS.get(returnedbook).then(response => {
          commit('SET_returnedbook', response.data)
          return response
      }).catch(error => {
          return error.response;
      })
      return res
  },
  },
  getters: {
    getReturnedBooksData(state) {
      return state.returnedbook;
  }
  },
  mutations: {
    SET_returnedbook(state, returnedbook) {
      state.returnedbook = returnedbook;
  },
  },
})